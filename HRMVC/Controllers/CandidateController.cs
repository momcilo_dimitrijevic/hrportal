﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HRMVC.Models;
using System.Text;
using DataAccess;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Net;

namespace HRMVC.Controllers
{
    public class CandidateController : BaseController
    {
        #region Candidate

        // GET: Get view and populate droplists later to be used
        public ActionResult Index()
        {
            ViewBag.Companies = DataContext.Companies.Select(c => new { c.CompanyID, c.Name }).ToList();
            ViewBag.Levels = DataContext.Levels.Select(e => new { e.LevelID, e.Name }).ToList();
            ViewBag.Expertises = DataContext.Expertises.Select(e => new { e.ExpertiseID, e.Name }).ToList();

            return View();
        }

        // GET: Get view and populate droplists later to be used
        [HttpGet]
        public async Task<ActionResult> CandidateDetails(int id)
        {
            ViewBag.CandidateID = id;

            if (!(await EvaluationHelpers.CheckCandidateByID(DataContext, id)))
            {
                Response.StatusCode = 404;
                return Content("Candidate not found");
            }

            ViewBag.Companies = DataContext.Companies.Select(c => new { c.CompanyID, c.Name }).ToList();
            ViewBag.Levels = DataContext.Levels.Select(e => new { e.LevelID, e.Name }).ToList();
            ViewBag.Expertises = DataContext.Expertises.Select(e => new { e.ExpertiseID, e.Name }).ToList();

            return View();
        }

        //Get candidate by id
        public async Task<JsonResult> GetCandidate(int id)
        {

            //check candidate id
            var candidate = await DataContext.Candidates.Where(x => x.CandidateID == id)
                                    .Select(c => new CandidateViewModel
                                    {
                                        CandidateID = c.CandidateID,
                                        FirstName = c.FirstName,
                                        LastName = c.LastName,
                                        Email = c.Email,
                                        Phone = c.Phone,
                                        LinkedIn = c.LinkedIn,
                                        City = c.City,
                                        ExpectedSalary = c.ExpectedSalary,
                                        CompanyID = c.CompanyID,
                                        CompanyName = c.Company.Name,
                                        School = c.School,
                                        Other = c.Other,
                                        Created = c.Created,
                                        CreatedBy = c.CreatedBy,
                                        LastUpdated = c.LastUpdated,
                                        LastUpdatedBy = c.LastUpdatedBy
                                    }).FirstOrDefaultAsync();

            //Get primary expertise
            var primaryExpertise = await DataContext.CandidatesExpertisesLevels
                        .Where(x => (x.CandidateID == id && x.IsPrimary == true && x.Deleted == null))
                        .Select(x => new ExpertiseLevelViewModel
                        {
                            ExpertiseID = x.ExpertiseID,
                            LevelID = x.LevelID,
                            LevelName = x.Level.Name,
                            ExpertiseName = x.Expertise.Name,
                            IsPrimary = x.IsPrimary
                        }).SingleOrDefaultAsync();

            candidate.PrimaryExpertise = primaryExpertise;

            //Get all secondary expertises related to candidate
            var secondaryExpertises = await DataContext.CandidatesExpertisesLevels
                        .Include(x => x.Expertise)
                        .Include(x => x.Level)
                        .Where(x => (x.CandidateID == id && x.IsPrimary != true && x.Deleted == null))
                        .Select(x => new ExpertiseLevelViewModel
                        {
                            ExpertiseID = x.ExpertiseID,
                            LevelID = x.LevelID,
                            LevelName = x.Level.Name,
                            ExpertiseName = x.Expertise.Name,
                            IsPrimary = x.IsPrimary
                        }).ToListAsync();

            //Attach expertises to Candidate view model
            candidate.SecondaryExpertises = secondaryExpertises;


            string baseURL = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
            var documents = await DataContext.Documents
                                 .Where(x => (x.CandidateID == id && x.Deleted == null))
                                 .Select(x => new DocumentViewModel
                                 {
                                     Name = x.Name,
                                     Created = x.Created,
                                     CreatedBy = x.CreatedBy,
                                     Link = baseURL + "Settings/Download/" + x.DocumentGUID.ToString(),
                                     DeleteLink = baseURL + "Settings/DeleteFile/" + x.DocumentGUID.ToString()
                                 })
                                 .ToListAsync();

            candidate.Documents = documents;

            var comments = await DataContext.Comments
                              .Where(c => c.CandidateID == id && c.Deleted == null)
                              .Select(c => new CommentViewModel
                              {
                                  CommentID = c.CommentID,
                                  CandidateID = c.CandidateID,
                                  Text = c.Comment1,
                                  CreatedBy = c.CreatedBy,
                                  Created = c.Created

                              })
                              .OrderByDescending(c => c.Created)
                              .ToListAsync();

            candidate.Comments = comments;

            var campaigns = await DataContext.CampaignsCandidatesStatuses
                                    .Include(x => x.Campaign)
                                    .Include(x => x.Status)
                                    .Where(c => c.CandidateID == candidate.CandidateID && c.Deleted == null && c.Campaign.DateFinished > DateTime.Now)
                                    .GroupBy(c => c.CampaignID)
                                    .Select(g => g.OrderByDescending(c => c.Created)
                                    .Select(y => new CandidateCampaignViewModel
                                    {
                                        Name = y.Campaign.Name,
                                        Description = y.Campaign.Description,
                                        Status = y.Status.Name
                                    }).FirstOrDefault()).ToListAsync();

            candidate.Campaigns = campaigns;

            return Json(candidate, JsonRequestBehavior.AllowGet);
        }

        //Get all candidates with details
        public async Task<JsonResult> GetCandidates()
        {
            var candidates = await DataContext.Candidates
                                    .Include(c => c.CandidatesExpertisesLevels.Select(a => a.Level))
                                    .Include(c => c.CandidatesExpertisesLevels.Select(a => a.Expertise))
                                    .Select(c => new CandidateViewModel
                                    {
                                        CandidateID = c.CandidateID,
                                        FirstName = c.FirstName,
                                        LastName = c.LastName,
                                        Email = c.Email,
                                        Phone = c.Phone,
                                        LinkedIn = c.LinkedIn,
                                        City = c.City,
                                        ExpectedSalary = c.ExpectedSalary,
                                        CompanyID = c.CompanyID,
                                        School = c.School,
                                        Created = c.Created,
                                        CreatedBy = c.CreatedBy,
                                        LastUpdated = c.LastUpdated,
                                        LastUpdatedBy = c.LastUpdatedBy,
                                        PrimaryExpertise = c.CandidatesExpertisesLevels
                                                            .Where(x => x.IsPrimary && x.Deleted == null)
                                                            .Select(x => new ExpertiseLevelViewModel
                                                            {
                                                                ExpertiseID = x.ExpertiseID,
                                                                LevelID = x.LevelID,
                                                                LevelName = x.Level.Name,
                                                                ExpertiseName = x.Expertise.Name,
                                                                IsPrimary = x.IsPrimary
                                                            }).FirstOrDefault(),

                                        SecondaryExpertises = c.CandidatesExpertisesLevels
                                                                .Where(x => !x.IsPrimary && x.Deleted == null)
                                                                .Select(x => new ExpertiseLevelViewModel
                                                                {
                                                                    ExpertiseID = x.ExpertiseID,
                                                                    LevelID = x.LevelID,
                                                                    LevelName = x.Level.Name,
                                                                    ExpertiseName = x.Expertise.Name,
                                                                    IsPrimary = x.IsPrimary
                                                                }).ToList()
                                    }).ToListAsync();

            return Json(candidates, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateModel]
        //[ValidateAntiForgeryToken]
        public async Task<JsonResult> CreateCandidate(CandidateViewModel candidate)
        {
            string message;

            if (candidate == null)
            {
                message = "Bad request: Invalid data sent from client";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            if (candidate.SecondaryExpertises
                    .Any(x => x.ExpertiseID == candidate.PrimaryExpertise.ExpertiseID))
            {
                message = "Error: Duplicate expertise found, primary expertise also specified as secondary";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            if (candidate.SecondaryExpertises.Count != candidate.SecondaryExpertises
                                                        .Select(x => x.ExpertiseID).Distinct().Count())
            {
                message = "Error: Duplicate expertise found, two secondary expertises have same expertise field";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            Candidate c = new Candidate();

            c.FirstName = candidate.FirstName;
            c.LastName = candidate.LastName;
            c.City = candidate.City;
            c.Email = candidate.Email;
            c.Phone = candidate.Phone;
            c.School = candidate.School;
            c.LinkedIn = candidate.LinkedIn;
            c.CompanyID = candidate.CompanyID;
            c.ExpectedSalary = candidate.ExpectedSalary;
            c.Created = DateTime.Now;
            c.CreatedBy = User.Identity.Name;
            c.Other = candidate.Other;

            DataContext.Candidates.Add(c);
            await DataContext.SaveChangesAsync();

            //create primary expertise
            CandidatesExpertisesLevel primExp = new CandidatesExpertisesLevel();

            primExp.CandidateID = c.CandidateID;
            primExp.ExpertiseID = candidate.PrimaryExpertise.ExpertiseID;
            primExp.LevelID = candidate.PrimaryExpertise.LevelID;
            primExp.IsPrimary = true;
            primExp.Created = DateTime.Now;
            primExp.CreatedBy = User.Identity.Name;

            DataContext.CandidatesExpertisesLevels.Add(primExp);

            List<CandidatesExpertisesLevel> newExpertises = new List<CandidatesExpertisesLevel>();

            foreach (var item in candidate.SecondaryExpertises)
            {
                newExpertises.Add(new CandidatesExpertisesLevel
                {
                    CandidateID = c.CandidateID,
                    ExpertiseID = item.ExpertiseID,
                    LevelID = item.LevelID,
                    Created = DateTime.Now,
                    CreatedBy = User.Identity.Name,
                    IsPrimary = true
            });
            }

            DataContext.CandidatesExpertisesLevels.AddRange(newExpertises);

            DataContext.Candidates.Add(c);


            await DataContext.SaveChangesAsync();

            

            message = "Success: Candidate " + c.FirstName + " " + c.LastName +  " created!";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateModel]
        public async Task<JsonResult> EditCandidate(CandidateViewModel candidate)
        {

            string message;

            if (candidate == null)
            {
                message = "Bad request: Invalid data sent from client";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            if (candidate.SecondaryExpertises !=null && candidate.SecondaryExpertises
                .Any(x => x.ExpertiseID == candidate.PrimaryExpertise.ExpertiseID))
            {
                message = "Error: Duplicate expertise found, primary expertise also specified as secondary";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            if (candidate.SecondaryExpertises != null && candidate.SecondaryExpertises.Count != candidate.SecondaryExpertises
                                                        .Select(x => x.ExpertiseID).Distinct().Count())
            {
                message = "Error: Duplicate expertise found, two secondary expertises have same expertise field";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            var c = await DataContext.Candidates
                             .Where(x => x.CandidateID == candidate.CandidateID)
                             .FirstOrDefaultAsync();

            if (c == null)
            {
                message = "Error: Could not update specified candidate, no candidates with matching ID found";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            //c.CandidateID = candidate.CandidateID;
            c.FirstName = candidate.FirstName;
            c.LastName = candidate.LastName;
            c.City = candidate.City;
            c.Email = candidate.Email;
            c.Phone = candidate.Phone;
            c.School = candidate.School;
            c.LinkedIn = candidate.LinkedIn;
            c.CompanyID = candidate.CompanyID;
            c.ExpectedSalary = candidate.ExpectedSalary;
            c.LastUpdated = DateTime.Now;
            c.LastUpdatedBy = User.Identity.Name;
            c.Other = candidate.Other;


            if (candidate.PrimaryExpertise != null)
            {
                //primary expertise create or edit
                var pe = await DataContext.CandidatesExpertisesLevels
                                                .Where(x => x.CandidateID == candidate.CandidateID && x.IsPrimary && x.Deleted == null)
                                                .SingleOrDefaultAsync();

                if (pe == null)
                {
                    CandidatesExpertisesLevel primExp = new CandidatesExpertisesLevel();

                    primExp.CandidateID = candidate.CandidateID;
                    primExp.ExpertiseID = candidate.PrimaryExpertise.ExpertiseID;
                    primExp.LevelID = candidate.PrimaryExpertise.LevelID;
                    primExp.IsPrimary = true;
                    primExp.Created = DateTime.Now;
                    primExp.CreatedBy = User.Identity.Name;

                    DataContext.CandidatesExpertisesLevels.Add(primExp);
                }
                else
                {
                    pe.ExpertiseID = candidate.PrimaryExpertise.ExpertiseID;
                    pe.LevelID = candidate.PrimaryExpertise.LevelID;
                }
            }


            if(candidate.SecondaryExpertises != null)
            {
                var deleteExpertises = await DataContext.CandidatesExpertisesLevels
                                .Where(x => x.CandidateID == candidate.CandidateID && !x.IsPrimary && x.Deleted == null).ToListAsync();

                if (deleteExpertises != null)
                {
                    deleteExpertises.ForEach(x => { x.Deleted = DateTime.Now; x.DeletedBy = User.Identity.Name; });
                }

                List<CandidatesExpertisesLevel> newExpertises = new List<CandidatesExpertisesLevel>();

                foreach (var item in candidate.SecondaryExpertises)
                {
                    newExpertises.Add(new CandidatesExpertisesLevel
                    {
                        CandidateID = candidate.CandidateID,
                        ExpertiseID = item.ExpertiseID,
                        LevelID = item.LevelID,
                        Created = DateTime.Now,
                        CreatedBy = User.Identity.Name,
                        IsPrimary = false
                    });
                }

                DataContext.CandidatesExpertisesLevels.AddRange(newExpertises);
            }

            await DataContext.SaveChangesAsync();

            message = "Success: Candidate: " + candidate.FirstName + " " + candidate.LastName + " updated!";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);


        }

        //Get candidate's expertise levels
        public async Task<JsonResult> GetExpertiseLevels(int id)
        {
            var expertises = await DataContext.CandidatesExpertisesLevels
                                    .Include(x => x.Expertise)
                                    .Include(x => x.Level)
                                    .Where(x => x.CandidateID == id)
                                    .Select(x => new ExpertiseLevelViewModel
                                    {
                                        ExpertiseID = x.ExpertiseID,
                                        LevelID = x.LevelID,
                                        IsPrimary = x.IsPrimary
                                    }).ToListAsync();

            return Json(expertises, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Comment

        //Create a comment for a candidate
        [HttpPost]
        [ValidateModel]
        public async Task<JsonResult> CreateComment(CommentViewModel comment)
        {
            string message;

            if (comment == null)
            {
                message = "Bad request: Invalid data sent from client";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            //check if candidate exists
            if (!(await EvaluationHelpers.CheckCandidateByID(DataContext, comment.CandidateID)))
            {
                message = "Error: Create comment failed, the candidate specified does not exist.";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            Comment c = new Comment();

            c.Comment1 = comment.Text;
            c.CreatedBy = User.Identity.Name;
            c.Created = DateTime.Now;
            c.CandidateID = comment.CandidateID;


            DataContext.Comments.Add(c);
            await DataContext.SaveChangesAsync();

            comment.Created = c.Created;
            comment.CreatedBy = c.CreatedBy;
            comment.CommentID = c.CommentID;

            message = "Success: Comment posted!";
            return Json(new { success = true, comment }, JsonRequestBehavior.AllowGet);
        }

        //Get comments for a specific candidate
        public async Task<JsonResult> GetComments(int id)
        {
            var comments = await DataContext.Comments
                               .Where(c => c.CandidateID == id && c.Deleted == null)
                               .Select(c => new CommentViewModel
                               {
                                   CommentID = c.CommentID,
                                   CandidateID = c.CandidateID,
                                   Text = c.Comment1,
                                   CreatedBy = c.CreatedBy,
                                   Created = c.Created

                               })
                               .OrderByDescending(c => c.Created)
                               .ToListAsync();
            return Json(comments, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> DeleteComment(int id)
        {
            string message;

            var c = await DataContext.Comments
                                 .Where(x => (x.CommentID == id && x.Deleted == null))
                                 .FirstOrDefaultAsync();

            if (c == null)
            {
                message = "Error: Entry with selected ID not found, or already deleted";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            if (User.Identity.Name != c.CreatedBy)
            {
                message = "Error: Current user does not have permission to delete comment.";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            c.Deleted = DateTime.Now;
            c.DeletedBy = User.Identity.Name;

            await DataContext.SaveChangesAsync();

            message = "Comment: Comment deleted!";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}