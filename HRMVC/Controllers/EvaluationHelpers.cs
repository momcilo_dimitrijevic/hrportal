﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccess;
using System.Threading.Tasks;
using System.Data.Entity;

namespace HRMVC.Controllers
{
    public static class EvaluationHelpers
    {

        //check if candidate specified by id exists
        public static async Task<bool> CheckCandidateByID(HRPortalDBEntities db, int id)
        {
            var candidate = await db.Candidates.FindAsync(id);

            if (candidate != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //check if campaign specified by id exists
        public static async Task<bool> CheckCampaignByID(HRPortalDBEntities db, int id)
        {
            var campaign = await db.Campaigns.FindAsync(id);

            if (campaign != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //check if campaign is finished
        public static async Task<bool> CheckCampaignFinished(HRPortalDBEntities db, int id)
        {
            var campaign = await db.Campaigns.FindAsync(id);

            if (campaign.DateFinished < DateTime.Now)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //check if candidate is assigned to campaign
        public static async Task<bool> CheckCandidateCampaignRelation(HRPortalDBEntities db, int candidateID, int campaignID)
        {
            var assignedCandidates = await db.CampaignsCandidatesStatuses
                                                    .Where(x => (x.CampaignID == campaignID && x.Deleted == null))
                                                    .Select(x => x.CandidateID)
                                                    .Distinct().ToListAsync();

            if (assignedCandidates.Contains(candidateID))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //check if status specified by id exists
        public static async Task<bool> CheckStatusByID(HRPortalDBEntities db, int id)
        {
            var status = await db.Statuses.FindAsync(id);

            if (status != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //check if expertise specified by id exists
        public static async Task<bool> CheckExpertiseByID(HRPortalDBEntities db, int id)
        {
            var expertise = await db.Expertises.FindAsync(id);

            if (expertise != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //check if comment specified by id exists
        public static async Task<bool> CheckCommentByID(HRPortalDBEntities db, int id)
        {
            var comment = await db.Comments.FindAsync(id);

            if (comment != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //check if company specified by id exists
        public static async Task<bool> CheckCompanyByID(HRPortalDBEntities db, int id)
        {
            var company = await db.Comments.FindAsync(id);

            if (company != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //check if document specified by guid exists
        public static async Task<bool> CheckDocumentByGUID(HRPortalDBEntities db, Guid id)
        {
            var document = await db.Documents.FindAsync(id);

            if (document != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //check if level specified by id exists
        public static async Task<bool> CheckLevelByID(HRPortalDBEntities db, int id)
        {
            var level = await db.Levels.FindAsync(id);

            if (level != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}