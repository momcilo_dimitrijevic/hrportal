﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using HRMVC.Models;
using System.Data.Entity;
using System.Net;
using DataAccess;

namespace HRMVC.Controllers
{
    public class CampaignController : BaseController
    {
        // VIEW Return Campaigns view with DataTables Grid
        public ActionResult Index()
        {
            return View();
        }

        // GET Return Campaign speicified by CampaignID value
        public async Task<JsonResult> GetCampaign(int? id)
        {

            if (id == null)
            {
                return Json(new HttpStatusCodeResult(HttpStatusCode.BadRequest), JsonRequestBehavior.AllowGet);
            }


            Campaign c = await DataContext.Campaigns.FindAsync(id);
            if (c == null)
            {
                return Json(HttpNotFound(), JsonRequestBehavior.AllowGet);
            }

            CampaignViewModel campaign = new CampaignViewModel();

            campaign.CampaignID = c.CampaignID;
            campaign.Name = c.Name;
            campaign.DateStarted = c.DateStarted;
            campaign.DateFinished = c.DateFinished;
            campaign.Description = c.Description;
            campaign.Created = c.Created;
            campaign.CreatedBy = c.CreatedBy;
            campaign.LastUpdated = c.LastUpdated;
            campaign.LastUpdatedBy = c.LastUpdatedBy;

            return Json(campaign, JsonRequestBehavior.AllowGet);
        }


        //GET Get all campaigns from campaigns DataSet
        public async Task<JsonResult> GetCampaigns()
        {
            var campaigns = await DataContext.Campaigns
                    .Select(c => new CampaignViewModel
                    {
                        CampaignID = c.CampaignID,
                        Name = c.Name,
                        DateStarted = c.DateStarted,
                        DateFinished = c.DateFinished,
                        Description = c.Description,
                        CreatedBy = c.CreatedBy,
                        LastUpdated = c.LastUpdated,
                        LastUpdatedBy = c.LastUpdatedBy
                    }).ToListAsync();

            return Json(campaigns, JsonRequestBehavior.AllowGet);
        }

        //GET Get all campaigns from campaigns DataSet with candidates and their current statuses
        public async Task<JsonResult> GetCampaignsComplete()
        {

            //get all campaigns with candidates and statuses
            var campaignsQuery = await DataContext.Campaigns
                            .Include(c => c.CampaignsCandidatesStatuses.Select(cd => cd.Candidate))
                            .Include(c => c.CampaignsCandidatesStatuses.Select(st => st.Status))
                            .Select(x => new CampaignViewModel
                            {
                                CampaignID = x.CampaignID,
                                DateStarted = x.DateStarted,
                                DateFinished = x.DateFinished,
                                Name = x.Name,
                                Created = x.Created,
                                CreatedBy = x.CreatedBy,
                                Description = x.Description,
                                LastUpdated = x.LastUpdated,
                                LastUpdatedBy = x.LastUpdatedBy,
                            })
                            .ToListAsync();

            return Json(campaignsQuery, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetAssignedCandidates(int? id)
        {
            string message;

            if (id == null)
            {
                message = "Bad request: Invalid data sent from client";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            //check if campaign exists
            if (!(await EvaluationHelpers.CheckCampaignByID(DataContext, id ?? default(int))))
            {
                message = "Error: Could not fetch candidates, no campaigns with matching ID found";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            List<AssignedCandidateViewModel> candidates = new List<AssignedCandidateViewModel>();

                candidates = DataContext.CampaignsCandidatesStatuses
                                    .Include(x => x.Candidate.CandidatesExpertisesLevels.Select(e => e.Expertise))
                                    .Where(c => (c.CampaignID == id && c.Deleted == null))
                                    .GroupBy(c => c.CandidateID)
                                    .Select(g => g.OrderByDescending(c => c.Created)
                                    .Select(y => new AssignedCandidateViewModel
                                    {
                                        CandidateID = y.CandidateID,
                                        FirstName = y.Candidate.FirstName,
                                        LastName = y.Candidate.LastName,
                                        LastStatus = y.Status.Name,
                                        PrimaryExpertise = y.Candidate.CandidatesExpertisesLevels.Where(x => x.IsPrimary).Select(x => x.Expertise.Name).FirstOrDefault()
                                    })
                                    .FirstOrDefault()).ToList();


            return Json(candidates, JsonRequestBehavior.AllowGet);
        }

        //POST Create new campaign
        //[ValidateAntiForgeryToken]
        [HttpPost]
        [ValidateModel]
        public async Task<JsonResult> CreateCampaign(CampaignViewModel campaign)
        {
            string message;

            if (campaign.DateStarted >= campaign.DateFinished)
            {
                message = "Error: Start date can't be later or on the same day as finish date!";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            Campaign c = new Campaign();

            c.DateStarted = campaign.DateStarted;
            c.DateFinished = campaign.DateFinished.AddHours(23).AddMinutes(59);
            c.Description = campaign.Description;
            c.CreatedBy = User.Identity.Name;
            c.Created = DateTime.Now;
            c.Name = campaign.Name;

            DataContext.Campaigns.Add(c);
            await DataContext.SaveChangesAsync();

            message = "Success: Campaign " + c.Name + " created!";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);
        }

        // POST Campaigns/EditCampaign/5 Edit Campaign
        [HttpPost]
        [ValidateModel]
        public async Task<JsonResult> EditCampaign(CampaignViewModel campaign)
        {

            string message;

            if (campaign == null)
            {
                message = "Bad request: Invalid data sent from client";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            if (campaign.DateStarted >= campaign.DateFinished)
            {
                message = "Error: Start date can't be later or on the same day as finish date!";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            var c = await DataContext.Campaigns
                                 .Where(x => x.CampaignID == campaign.CampaignID)
                                 .FirstOrDefaultAsync();

                if (c == null)
                {
                    message = "Error: Could not update specified campaign, no campaigns with matching ID found";
                    return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
                }

                c.Name = campaign.Name;
                c.DateStarted = campaign.DateStarted;
                c.DateFinished = campaign.DateFinished.AddHours(23).AddMinutes(59);
                c.Description = campaign.Description;
                c.LastUpdated = DateTime.Now;
                c.LastUpdatedBy = User.Identity.Name;

                await DataContext.SaveChangesAsync();

            message = "Success: Campaign " + c.Name + " updated!";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Assign()
        {
            return View();
        }

        //Return valid statuses that a candidate can have in a campaign
        [HttpPost]
        [ValidateModel]
        public async Task<JsonResult> ReturnValidStatuses(CampaignStatusAssignmentViewModel assignment)
        {
            string message;

            var currentCampaign = await DataContext.Campaigns
                                                    .Where(c => c.CampaignID == assignment.CampaignID)
                                                    .SingleOrDefaultAsync();


            //check if campaign is finished
            if (await EvaluationHelpers.CheckCampaignFinished(DataContext, currentCampaign.CampaignID))
            {
                message = "Campaign finished, no changes in this campaign can be made now";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            var mostRecentStatus = await DataContext.CampaignsCandidatesStatuses
                                                .Include(e => e.Status)
                                                .Where(c => (c.CandidateID == assignment.CandidateID && c.CampaignID == assignment.CampaignID))
                                                .OrderByDescending(a => a.Created)
                                                .FirstOrDefaultAsync();

            var statuses = await DataContext.Statuses
                                .Where(s => s.Priority > mostRecentStatus.Status.Priority)
                                .Select(s => new StatusViewModel
                                {
                                    StatusID = s.StatusID,
                                    Name = s.Name,
                                    Priority = s.Priority
                                })
                                .ToListAsync();

            if (statuses.Count == 0)
            {
                message = "No additional valid statuses available for current user";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = true, statuses }, JsonRequestBehavior.AllowGet);

        }

        //Add new status for a candidate in a campaign
        [HttpPost]
        [ValidateModel]
        //[ValidateAntiForgeryToken]
        public async Task<JsonResult> AddCandidateStatus(CampaignStatusAssignmentViewModel assignment)
        {
            string message;

            //check if candidate exists
            if (!(await EvaluationHelpers.CheckCandidateByID(DataContext, assignment.CandidateID)))
            {
                message = "Error: Status update failed, the candidate specified does not exist.";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            //check if campaign exists
            if (!(await EvaluationHelpers.CheckCampaignByID(DataContext, assignment.CampaignID)))
            {
                message = "Error: Status update failed, the campaign specified does not exist.";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            //check if campaign is finished
            if (await EvaluationHelpers.CheckCampaignFinished(DataContext, assignment.CampaignID))
            {
                message = "Error: Status update failed, campaign is finished. No changes in this campaign can be made now";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            //check if status exists
            if (!(await EvaluationHelpers.CheckStatusByID(DataContext, assignment.StatusID)))
            {
                message = "Error: Status update failed, the status specified does not exist.";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            //check if candidate is assigned to campaign
            if (!(await EvaluationHelpers.CheckCandidateCampaignRelation(DataContext, assignment.CandidateID, assignment.CampaignID)))
            {
                message = "Error: Status update failed, candidate is not assigned to current campaign";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            //get most recent status
            var mostRecentStatus = await DataContext.CampaignsCandidatesStatuses
                                                    .Include(s => s.Status)
                                                    .Where(a => (a.CandidateID == assignment.CandidateID && a.CampaignID == assignment.CampaignID))
                                                    .OrderByDescending(b => b.Created)
                                                    .FirstOrDefaultAsync();

            //get assigned status to check priority against the most recent status
            var assignmentStatus = await DataContext.Statuses
                                                .Where(s => s.StatusID == assignment.StatusID)
                                                .SingleOrDefaultAsync();

            //check if Status priority is greater than the latest status priority
            if (mostRecentStatus.Status.Priority > assignmentStatus.Priority)
            {
                message = "Error: Invalid status priority";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            CampaignsCandidatesStatus c = new CampaignsCandidatesStatus();

            c.CampaignID = assignment.CampaignID;
            c.CandidateID = assignment.CandidateID;
            c.StatusID = assignment.StatusID;
            c.Created = DateTime.Now;
            c.CreatedBy = User.Identity.Name;

            DataContext.CampaignsCandidatesStatuses.Add(c);
            await DataContext.SaveChangesAsync();

            message = "Success: Candidate status updated!";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        [ValidateModel]
        public async Task<JsonResult> DeleteCampaignCandidate(CampaignStatusAssignmentViewModel assignment)
        {
            string message;

            //check if candidate exists
            if (!(await EvaluationHelpers.CheckCandidateByID(DataContext, assignment.CandidateID)))
            {
                message = "Error: Delete failed, the candidate specified does not exist.";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            //check if campaign exists
            if (!(await EvaluationHelpers.CheckCampaignByID(DataContext, assignment.CampaignID)))
            {
                message = "Error: Delete failed, the campaign specified does not exist.";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            //check if campaign is finished
            if (await EvaluationHelpers.CheckCampaignFinished(DataContext, assignment.CampaignID))
            {
                message = "Error: Status update failed, campaign is finished. No changes in this campaign can be made now";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            //check if candidate is assigned to campaign
            if (!(await EvaluationHelpers.CheckCandidateCampaignRelation(DataContext, assignment.CandidateID, assignment.CampaignID)))
            {
                message = "Error: Delete failed, candidate is not assigned to current campaign.";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            var candidateCampaignStatuses = await DataContext.CampaignsCandidatesStatuses
                                                            .Where(x => (x.CampaignID == assignment.CampaignID
                                                                            && x.CandidateID == assignment.CandidateID
                                                                            && x.Deleted == null))
                                                            .ToListAsync();

            foreach (var item in candidateCampaignStatuses)
            {
                item.Deleted = DateTime.Now;
                item.DeletedBy = User.Identity.Name;
            }

            await DataContext.SaveChangesAsync();

            message = "Success: Candidate deleted from campaign successfully.";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);
        }

        
        public async Task<JsonResult> GetUnassignedCandidates(int id)
        {
            string message;

            //check if campaign exists
            if (!(await EvaluationHelpers.CheckCampaignByID(DataContext, id)))
            {
                message = "Error: Cannot get candidates, campaign does not exist";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            
            //check if campaign is finished
            if (await EvaluationHelpers.CheckCampaignFinished(DataContext, id))
            {
                message = "Error: Cannot get candidates, campaign is finished.";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            //all asignments for this campaign
            var candidateAssignments = DataContext.CampaignsCandidatesStatuses.Where(c => (c.CampaignID == id && c.Deleted == null));

            var unassignedCandidates = await DataContext.Candidates
                                                    .Where(c => !candidateAssignments
                                                        .Select(b => b.CandidateID)
                                                        .Contains(c.CandidateID)
                                                    )
                                                    .OrderByDescending(c=> c.Created)
                                                    .Select(x => new AssignedCandidateViewModel
                                                    {
                                                        CandidateID = x.CandidateID,
                                                        FirstName = x.FirstName,
                                                        LastName = x.LastName
                                                    }).ToListAsync();

            if (unassignedCandidates.Count == 0)
            {
                message = "Error: No unassigned candidates currently exist for selected campaign.";
                return Json(new {success = false, message }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = true, unassignedCandidates }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateModel]
        public async Task<JsonResult> AssignCandidates(AssignCandidatesViewModel assignment)
        {
            int candidatesAssigned = 0;
            List<string> messages = new List<string>();

            if (assignment.CandidateIDs.Count == 1 && assignment.CandidateIDs.First()==0)
            {
                messages.Add("Error: No candidates were specified.");
                return Json(new { success = false, messages }, JsonRequestBehavior.AllowGet);
            }

            //check if campaign exists
            if (!(await EvaluationHelpers.CheckCampaignByID(DataContext, assignment.CampaignID)))
            {
                messages.Add("Error: Delete failed, the campaign specified does not exist.");
                return Json(new { success = false, messages }, JsonRequestBehavior.AllowGet);
            }

            //check if campaign is finished
            if (await EvaluationHelpers.CheckCampaignFinished(DataContext, assignment.CampaignID))
            {
                messages.Add("Error: Status update failed, campaign is finished. No changes in this campaign can be made now");
                return Json(new { success = false, messages }, JsonRequestBehavior.AllowGet);
            }

            List<Candidate> validCandidates = new List<Candidate>();

            //add to validCandidates list only those candidetes that pass evaluation
            foreach (var item in assignment.CandidateIDs)
            {
                var candidate = await DataContext.Candidates.FindAsync(item);
                //check if candidate exists
                if (candidate == null)
                {
                    messages.Add("Candidate with ID: " + item + " does not exist.");
                }
                //check if candidate is already assigned to campaign
                else if (await EvaluationHelpers.CheckCandidateCampaignRelation(DataContext, item, assignment.CampaignID))
                {
                    messages.Add("Candidate " + candidate.FirstName + " " + candidate.LastName + " is already in this campaign.");
                }
                else
                {
                    validCandidates.Add(candidate);
                    candidatesAssigned++;
                }
            }

            if (candidatesAssigned == 0)
            {
                messages.Add("Error: No candidates were assigned to campaign");
                return Json(new { success = false, messages }, JsonRequestBehavior.AllowGet);
            }

            //add valid candidates to campaign
            foreach (var item in validCandidates)
            {
                DataContext.CampaignsCandidatesStatuses
                                        .Add(new CampaignsCandidatesStatus
                                        {
                                            CampaignID = assignment.CampaignID,
                                            CandidateID = item.CandidateID,
                                            StatusID = 1,
                                            Created = DateTime.Now,
                                            CreatedBy = User.Identity.Name
                                        });

                
                messages.Add("Candidate " + item.FirstName + " " + item.LastName + " added successfully.");
            }

            await DataContext.SaveChangesAsync();
            return Json(new { success = true, messages }, JsonRequestBehavior.AllowGet);
        }
    }
}