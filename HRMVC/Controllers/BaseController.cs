﻿using System.Web.Mvc;
using DataAccess;
using HRMVC.Models;

namespace HRMVC.Controllers
{
    [Authorize]
    public abstract class BaseController : AsyncController
    {
        private HRPortalDBEntities db = null;

        public HRPortalDBEntities DataContext
        {
            get
            {
                if (db == null)
                {
                    db = new HRPortalDBEntities();
                }

                return db;
            }
        }

    }
}