﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRMVC.Controllers
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            string errors = null;

            var modelState = filterContext.Controller.ViewData.ModelState;
            if (!modelState.IsValid)
            {

                errors = string.Join("</br>", modelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));

                var errorModel = new { success = false, message = errors };

                filterContext.Result = new JsonResult()
                {
                    Data = errorModel
                };
            }


        }
    }
}