﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccess;
using HRMVC.Models;
using System.Data.Entity.Infrastructure;
using Microsoft.AspNet.Identity;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Net;

namespace HRMVC.Controllers
{
    public class SettingsController : BaseController
    {

        public ActionResult ChangeTheme(string themeName)
        {
            Session["CssTheme"] = themeName;

            if (Request.UrlReferrer != null)
            {
                var returnUrl = Request.UrlReferrer.ToString();
                return new RedirectResult(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult ChangeNavbarType(string navbarType)
        {
            Session["NavbarType"] = navbarType;

            if (Request.UrlReferrer != null)
            {
                var returnUrl = Request.UrlReferrer.ToString();
                return new RedirectResult(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }

        #region Files
        // GET: Settings
        public ActionResult Index()
        {
            return View();
        }

        // POST Upload file via default form, no Ajax
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Upload(HttpPostedFileBase upload)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var doc = new Document();
                    if (upload != null && upload.ContentLength > 0)
                    {
                        doc.Name = System.IO.Path.GetFileName(upload.FileName);
                        doc.Created = DateTime.Now;
                        doc.CandidateID = 5;
                        doc.CreatedBy = User.Identity.Name;

                        using (var reader = new System.IO.BinaryReader(upload.InputStream))
                        {
                            doc.Content = reader.ReadBytes(upload.ContentLength);
                        }
                    }

                    DataContext.Documents.Add(doc);
                    DataContext.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (RetryLimitExceededException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return View();
        }

        // GET Download a file specified by GUID argument
        public async Task<ActionResult> Download(Guid id)
        {
            var doc = await DataContext.Documents.Where(x => x.DocumentGUID.Equals(id)).FirstOrDefaultAsync();

            if (doc == null)
            {
                return HttpNotFound("File not found");
            }

            string filename = doc.Name;
            byte[] filedata = doc.Content;
            string contentType = MimeMapping.GetMimeMapping(filename);

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = filename,
                DispositionType = DispositionTypeNames.Attachment
            };

            Response.AppendHeader("Content-Disposition", cd.ToString());

            return File(filedata, contentType);
        }

        // POST Upload multiple documents via Ajax
        public JsonResult UploadFiles()
        {
            string message = "Upload summary: \n";
            int candidateID = Int32.Parse(Request.Params.Get("CandidateID"));

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                                                            //Use the following properties to get file's name, size and MIMEType
                var doc = new Document();
                doc.CandidateID = candidateID;

                //check if CandidateID is valid?

                if (file != null && file.ContentLength > 0)
                {
                    doc.DocumentGUID = System.Guid.NewGuid();
                    doc.Name = System.IO.Path.GetFileName(file.FileName);
                    doc.Created = DateTime.Now;

                    doc.CreatedBy = User.Identity.Name;

                    using (var reader = new System.IO.BinaryReader(file.InputStream))
                    {
                        doc.Content = reader.ReadBytes(file.ContentLength);
                    }

                    message += "File succesfully uploaded: " + file.FileName + "\n";
                }
                else
                {
                    message += "Error adding file: " + file.FileName + ", file is empty. \n";
                }

                DataContext.Documents.Add(doc);
                DataContext.SaveChanges();
            }

            message = "File upload finished.";
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        //Delete file specified by file Guid
        public async Task<JsonResult> DeleteFile(Guid id)
        {
            string message;

            var d = await DataContext.Documents
                                 .Where(x => (x.DocumentGUID == id && x.Deleted == null))
                                 .FirstOrDefaultAsync();

            if (d == null)
            {
                message = "Error: Entry with selected ID not found, or already deleted";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            d.Deleted = DateTime.Now;
            d.DeletedBy = User.Identity.Name;

            await DataContext.SaveChangesAsync();

            message = "Success: File: " + d.Name + " deleted!";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost] Restore deleted file
        public async Task<JsonResult> RestoreFile(Guid id)
        {
            string message;

            var d = await DataContext.Documents
                                 .Where(x => (x.DocumentGUID == id && x.Deleted != null))
                                 .FirstOrDefaultAsync();

            if (d == null)
            {
                message = "Error: Entry with selected ID not found, or already restored";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            d.Deleted = null;
            d.DeletedBy = null;

            await DataContext.SaveChangesAsync();

            message = "Success: Document with ID: " + d.DocumentID + " restored!";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);
        }

        //Returns links for all files belonging to a certain candidate
        public async Task<JsonResult> GetFiles(int id)
        {
            string message;


            if(await DataContext.Candidates.FindAsync(id) == null)
            {
                message = "No candidates exist with specified ID.";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            var d = await DataContext.Documents
                                 .Where(x => (x.CandidateID == id && x.Deleted == null))
                                 .ToListAsync();

            if (d == null)
            {
                message = "There are currently no files for this candidate.";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            string baseURL = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
            var documents = await DataContext.Documents
                                 .Where(x => (x.CandidateID == id && x.Deleted == null))
                                 .Select(x => new DocumentViewModel
                                 {
                                     Name = x.Name,
                                     Created = x.Created,
                                     CreatedBy = x.CreatedBy,
                                     Link = baseURL + "Settings/Download/" + x.DocumentGUID.ToString(),
                                     DeleteLink = baseURL + "Settings/DeleteFile/" + x.DocumentGUID.ToString(),
                                 })
                                 .ToListAsync();

            return Json(new { success = true, documents }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Companies

        // VIEW Return Companies view with DataTables Grid
        public ActionResult Companies()
        {
            return View();
        }

        // GET Return Company speicified by CompanyID value
        public async Task<JsonResult> GetCompany(int? id)
        {

            if (id == null)
            {
                return Json(new HttpStatusCodeResult(HttpStatusCode.BadRequest), JsonRequestBehavior.AllowGet);
            }

            Company c = await DataContext.Companies.FindAsync(id);

            if (c == null)
            {
                return Json(HttpNotFound(), JsonRequestBehavior.AllowGet);
            }

            CompanyViewModel company = new CompanyViewModel();

            company.CompanyID = c.CompanyID;
            company.Name = c.Name;
            company.Address = c.Address;
            company.TelNum = c.TelNum;
            company.Website = c.Website;
            company.Created = c.Created;
            company.CreatedBy = c.CreatedBy;

            return Json(company, JsonRequestBehavior.AllowGet);
        }


        //GET Get all companies from Companies DataSet
        public async Task<JsonResult> GetCompanies()
        {
            var companies = await DataContext.Companies
                    .Where(x => x.Deleted == null)
                    .Select(c => new CompanyViewModel
                    {
                        CompanyID = c.CompanyID,
                        Address = c.Address,
                        Name = c.Name,
                        Created = c.Created,
                        CreatedBy = c.CreatedBy,
                        TelNum = c.TelNum,
                        Website = c.Website
                    }).ToListAsync();

            return Json(companies, JsonRequestBehavior.AllowGet);
        }

        //POST Create new company
        [HttpPost]
        [ValidateModel]
        //[ValidateAntiForgeryToken]
        public async Task<JsonResult> CreateCompany(CompanyViewModel company)
        {
            string message;

            if (company == null)
            {
                message = "Bad request: Invalid data sent from client";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            Company c = new Company();

            c.Name = company.Name;
            c.Address = company.Address;
            c.TelNum = company.TelNum;
            c.Website = company.Website;
            c.CreatedBy = User.Identity.Name;
            c.Created = DateTime.Now;

            DataContext.Companies.Add(c);
            await DataContext.SaveChangesAsync();

            message = "Success: Company " + c.Name + " created!";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);
        }

        // POST Settings/EditCompany/5 Edit Company
        [HttpPost]
        [ValidateModel]
        public async Task<JsonResult> EditCompany(CompanyViewModel company)
        {

            string message;

            if (company == null)
            {
                message = "Bad request: Invalid data sent from client";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            if (ModelState.IsValid)
            {

                var c = await DataContext.Companies
                                 .Where(x => x.CompanyID == company.CompanyID)
                                 .FirstOrDefaultAsync();

                if (c == null)
                {
                    message = "Error: Could not update specified Company, no companies with matching ID found";
                    return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
                }

                //c.CandidateID = candidate.CandidateID;
                c.Name = company.Name;
                c.Website = company.Website;
                c.TelNum = company.TelNum;
                c.Address = company.Address;

                await DataContext.SaveChangesAsync();
            }

            message = "Success: Company " + company.Name + " updated!";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);
        }

        // POST Delete company with specified id
        [HttpPost]
        public async Task<JsonResult> DeleteCompany(int id)
        {
            string message;

            var c = await DataContext.Companies
                                 .Where(x => (x.CompanyID == id && x.Deleted == null))
                                 .FirstOrDefaultAsync();

            if (c == null)
            {
                message = "Error: Entry with selected ID not found, or already deleted";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            c.Deleted = DateTime.Now;
            c.DeletedBy = User.Identity.Name;

            await DataContext.SaveChangesAsync();

            message = "Success: Company with ID: " + c.CompanyID + " deleted!";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);
        }

        // GET Returns all deleted companies
        public async Task<JsonResult> GetDeletedCompanies()
        {
            var companies = await DataContext.Companies
                    .Where(x => x.Deleted != null)
                    .Select(c => new CompanyViewModel
                    {
                        CompanyID = c.CompanyID,
                        Address = c.Address,
                        Name = c.Name,
                        Created = c.Created,
                        CreatedBy = c.CreatedBy,
                        TelNum = c.TelNum,
                        Website = c.Website
                    }).ToListAsync();

            return Json(companies, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost] ??? Restore deleted company
        public async Task<JsonResult> RestoreCompany(int id)
        {
            string message;

            var c = await DataContext.Companies
                                 .Where(x => (x.CompanyID == id && x.Deleted != null))
                                 .FirstOrDefaultAsync();

            if (c == null)
            {
                message = "Error: Entry with selected ID not found, or already restored";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            c.Deleted = null;
            c.DeletedBy = null;

            await DataContext.SaveChangesAsync();

            message = "Success: Company with ID: " + c.CompanyID + " restored!";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Expertises

        // VIEW Return Expertises view with DataTables Grid
        public ActionResult Expertises()
        {
            return View();
        }

        // GET Return Expertise speicified by ExpertiseID value
        public async Task<JsonResult> GetExpertise(int? id)
        {

            if (id == null)
            {
                return Json(new HttpStatusCodeResult(HttpStatusCode.BadRequest), JsonRequestBehavior.AllowGet);
            }

            Expertise e = await DataContext.Expertises.FindAsync(id);

            if (e == null)
            {
                return Json(HttpNotFound(), JsonRequestBehavior.AllowGet);
            }

            ExpertiseViewModel expertise = new ExpertiseViewModel();

            expertise.ExpertiseID = e.ExpertiseID;
            expertise.Name = e.Name;

            return Json(expertise, JsonRequestBehavior.AllowGet);
        }

        //GET Get all expertises from Expertises DataSet
        public async Task<JsonResult> GetExpertises()
        {
            var expertises = await DataContext.Expertises
                    .Select(e => new ExpertiseViewModel
                    {
                        ExpertiseID = e.ExpertiseID,
                        Name = e.Name
                    }).ToListAsync();

            return Json(expertises, JsonRequestBehavior.AllowGet);
        }

        //POST Create new Expertise
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<JsonResult> CreateExpertise(ExpertiseViewModel expertise)
        {
            string message;

            if (expertise == null)
            {
                message = "Bad request: Invalid data sent from client";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }


            if (!ModelState.IsValid)
            {
                message = string.Join("<br>", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));


                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            Expertise e = new Expertise();

            e.Name = expertise.Name;

            DataContext.Expertises.Add(e);
            await DataContext.SaveChangesAsync();

            message = "Success: Expertise created!";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);
        }

        // POST Settings/EditExpertise/5 Edit Expertise
        [HttpPost]
        public async Task<JsonResult> EditExpertise(ExpertiseViewModel expertise)
        {

            string message;

            if (expertise == null)
            {
                message = "Bad request: Invalid data sent from client";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }


            if (!ModelState.IsValid)
            {
                message = string.Join(" <br>", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));


                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            if (ModelState.IsValid)
            {

                var e = await DataContext.Expertises
                                 .Where(x => x.ExpertiseID == expertise.ExpertiseID)
                                 .FirstOrDefaultAsync();

                if (e == null)
                {
                    message = "Error: Could not update specified Expertise, no companies with matching ID found";
                    return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
                }

                e.Name = expertise.Name;

                await DataContext.SaveChangesAsync();
            }

            message = "Success: Expertise with ID: " + expertise.ExpertiseID + " updated!";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);
        }

        // POST Delete Expertise with specified id
        [HttpPost]
        public async Task<JsonResult> DeleteExpertise(int id)
        {
            string message;

            var e = await DataContext.Expertises
                                 .Where(x => x.ExpertiseID == id)
                                 .FirstOrDefaultAsync();

            if (e == null)
            {
                message = "Error: Entry with selected ID not found, or already deleted";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            DataContext.Expertises.Remove(e);
            await DataContext.SaveChangesAsync();

            message = "Success: Expertise with ID: " + e.ExpertiseID + " deleted!";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Levels

        // VIEW Return Levels view with DataTables Grid
        public ActionResult Levels()
        {
            return View();
        }

        //POST Create new level
        [HttpPost]
        [ValidateModel]
        //[ValidateAntiForgeryToken]
        public async Task<JsonResult> CreateLevel(LevelViewModel level)
        {
            string message;

            if (level == null)
            {
                message = "Bad request: Invalid data sent from client";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            Level l = new Level();

            l.Name = level.Name;

            DataContext.Levels.Add(l);
            await DataContext.SaveChangesAsync();

            message = "Success: level created!";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);
        }

        // GET Return Level speicified by LevelID value
        public async Task<JsonResult> GetLevel(int? id)
        {

            if (id == null)
            {
                return Json(new HttpStatusCodeResult(HttpStatusCode.BadRequest), JsonRequestBehavior.AllowGet);
            }

            Level l = await DataContext.Levels.FindAsync(id);

            if (l == null)
            {
                return Json(HttpNotFound(), JsonRequestBehavior.AllowGet);
            }

            LevelViewModel level = new LevelViewModel();

            level.LevelID = l.LevelID;
            level.Name = l.Name;

            return Json(level, JsonRequestBehavior.AllowGet);
        }

        //GET Get all levels from Levels DataSet
        public async Task<JsonResult> GetLevels()
        {
            var levels = await DataContext.Levels
                    .Select(l => new LevelViewModel
                    {
                        LevelID = l.LevelID,
                        Name = l.Name
                    }).ToListAsync();

            return Json(levels, JsonRequestBehavior.AllowGet);
        }

        //POST Create new level
        [HttpPost]
        [ValidateModel]
        //[ValidateAntiForgeryToken]
        public async Task<JsonResult> LevelExpertise(LevelViewModel level)
        {
            string message;

            if (level == null)
            {
                message = "Bad request: Invalid data sent from client";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            Level l = new Level();

            l.Name = level.Name;

            DataContext.Levels.Add(l);
            await DataContext.SaveChangesAsync();

            message = "Success: Expertise created!";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);
        }

        // POST Settings/EditLevel/5 Edit Level
        [HttpPost]
        [ValidateModel]
        public async Task<JsonResult> EditLevel(LevelViewModel level)
        {

            string message;

            if (level == null)
            {
                message = "Bad request: Invalid data sent from client";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            if (ModelState.IsValid)
            {

                var l = await DataContext.Levels
                                 .Where(x => x.LevelID == level.LevelID)
                                 .FirstOrDefaultAsync();

                if (l == null)
                {
                    message = "Error: Could not update specified Level, no levels with matching ID found";
                    return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
                }

                l.Name = level.Name;

                await DataContext.SaveChangesAsync();
            }

            message = "Success: Level with ID: " + level.LevelID + " updated!";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);
        }

        // POST Delete level with specified id
        [HttpPost]
        public async Task<JsonResult> DeleteLevel(int id)
        {
            string message;

            var l = await DataContext.Levels
                                 .Where(x => x.LevelID == id)
                                 .FirstOrDefaultAsync();

            if (l == null)
            {
                message = "Error: Entry with selected ID not found, or already deleted";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }


            DataContext.Levels.Remove(l);
            await DataContext.SaveChangesAsync();

            message = "Success: Level with ID: " + l.LevelID + " deleted!";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Statuses

        // VIEW Return Statuses view with DataTables Grid
        public ActionResult Statuses()
        {
            return View();
        }

        //POST Create new status
        [HttpPost]
        [ValidateModel]
        //[ValidateAntiForgeryToken]
        public async Task<JsonResult> CreateStatus(StatusViewModel status)
        {
            string message;

            if (status == null)
            {
                message = "Bad request: Invalid data sent from client";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            Status s = new Status();

            s.Name = status.Name;
            s.Priority = status.Priority;

            DataContext.Statuses.Add(s);
            await DataContext.SaveChangesAsync();

            message = "Success: status created!";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);
        }

        // GET Return Status speicified by StatusID value
        public async Task<JsonResult> GetStatus(int? id)
        {

            if (id == null)
            {
                return Json(new HttpStatusCodeResult(HttpStatusCode.BadRequest), JsonRequestBehavior.AllowGet);
            }

            Status s = await DataContext.Statuses.FindAsync(id);

            if (s == null)
            {
                return Json(HttpNotFound(), JsonRequestBehavior.AllowGet);
            }

            StatusViewModel status = new StatusViewModel();

            status.StatusID = s.StatusID;
            status.Name = s.Name;
            status.Priority = s.Priority;

            return Json(status, JsonRequestBehavior.AllowGet);
        }

        //GET Get all statuses from Statuses DataSet
        public async Task<JsonResult> GetStatuses()
        {
            var statuses = await DataContext.Statuses
                    .Select(s => new StatusViewModel
                    {
                        StatusID = s.StatusID,
                        Name = s.Name,
                        Priority = s.Priority  
                    }).ToListAsync();

            return Json(statuses, JsonRequestBehavior.AllowGet);
        }

        // POST Settings/EditStatus/5 Edit Status
        [HttpPost]
        [ValidateModel]
        public async Task<JsonResult> EditStatus(StatusViewModel status)
        {

            string message;

            if (status == null)
            {
                message = "Bad request: Invalid data sent from client";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }

            if (ModelState.IsValid)
            {

                var s = await DataContext.Statuses
                                 .Where(x => x.StatusID == status.StatusID)
                                 .FirstOrDefaultAsync();

                if (s == null)
                {
                    message = "Error: Could not update specified Level, no levels with matching ID found";
                    return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
                }

                s.Name = status.Name;
                s.Priority = status.Priority;

                await DataContext.SaveChangesAsync();
            }

            message = "Success: Status with ID: " + status.StatusID + " updated!";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);
        }

        // POST Delete status with specified id
        [HttpPost]
        public async Task<JsonResult> DeleteStatus(int id)
        {
            string message;

            var s = await DataContext.Statuses
                                 .Where(x => x.StatusID == id)
                                 .FirstOrDefaultAsync();

            if (s == null)
            {
                message = "Error: Entry with selected ID not found, or already deleted";
                return Json(new { success = false, message }, JsonRequestBehavior.AllowGet);
            }


            DataContext.Statuses.Remove(s);
            await DataContext.SaveChangesAsync();

            message = "Success: Status with ID: " + id + " deleted!";
            return Json(new { success = true, message }, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}