﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HRMVC.Models
{
    public class AssignCandidatesViewModel
    {
        public AssignCandidatesViewModel()
        {
            CandidateIDs = new List<int>();
        }

        [Required]
        public int CampaignID { get; set; }

        public List<int> CandidateIDs { get; set; }
    }
}