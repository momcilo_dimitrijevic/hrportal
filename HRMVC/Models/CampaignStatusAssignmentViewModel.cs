﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HRMVC.Models
{
    //CampaignCandidateStatus
    public class CampaignStatusAssignmentViewModel
    {
        public int ID { get; set; }
        [Required]
        public int CampaignID { get; set; }
        [Required]
        public int CandidateID { get; set; }
        public int StatusID { get; set; }

        public System.DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> Deleted { get; set; }
        public string DeletedBy { get; set; }
    }
}