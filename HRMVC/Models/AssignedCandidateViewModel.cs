﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRMVC.Models
{
    public class AssignedCandidateViewModel
    {
        public int CandidateID { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string PrimaryExpertise { get; set; }

        public string LastStatus { get; set; }
    }
}