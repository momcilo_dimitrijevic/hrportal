﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HRMVC.Models
{
    public class CommentViewModel
    {
        public int CommentID { get; set; }

        [Required(ErrorMessage = "No candidate specified to add comment for")]
        public int CandidateID { get; set; }

        [Required(ErrorMessage = "No blank comments are allowed")]
        public string Text { get; set; }

        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public DateTime Deleted { get; set; }
        public string DeletedBy { get; set; }
    }
}