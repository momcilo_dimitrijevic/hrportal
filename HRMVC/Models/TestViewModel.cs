﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRMVC.Models
{
    public class TestViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public HttpPostedFileBase CV { get; set; }
    }
}