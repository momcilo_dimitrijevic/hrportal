﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRMVC.Models
{
    public class DocumentViewModel
    {
        public string Name { get; set; }
        public string Link { get; set; }
        public string DeleteLink { get; set; }

        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
    }
}