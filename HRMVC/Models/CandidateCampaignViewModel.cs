﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRMVC.Models
{
    public class CandidateCampaignViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public string Status { get; set; }
    }
}