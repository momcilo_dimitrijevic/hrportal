﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HRMVC.Models
{
    public class ExpertiseViewModel
    {
        public int ExpertiseID { get; set; }

        [Required(ErrorMessage = "Expertise name is required")]
        public string Name { get; set; }
    }
}