﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using HRMVC.Helpers;

namespace HRMVC.Models
{
    public class CandidateViewModel
    {
        public CandidateViewModel()
        {}

        public int CandidateID { get; set; }
        [Required(ErrorMessage = "First name is required")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last name is required")]
        public string LastName { get; set; }
        public Nullable<int> CompanyID { get; set; }
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "E-mail is required")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Telephone number is required")]
        [RegularExpression(DisplayPatterns.PhonePattern, ErrorMessage = DisplayPatterns.PhonePatternMessage)]
        public string Phone { get; set; }

        public string LinkedIn { get; set; }
        [Required(ErrorMessage = "City field is required")]
        public string City { get; set; }
        [Required(ErrorMessage = "School field is required")]
        public string School { get; set; }
        [Required(ErrorMessage = "Expected salary field is required")]
        public Nullable<decimal> ExpectedSalary { get; set; }

        public string Other { get; set; }

        public System.DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public string LastUpdatedBy { get; set; }

        public ExpertiseLevelViewModel PrimaryExpertise { get; set; }

        public List<CommentViewModel> Comments { get; set; }
        public List<DocumentViewModel> Documents { get; set; }
        public List<CandidateCampaignViewModel> Campaigns { get; set; }
        public List<ExpertiseLevelViewModel> SecondaryExpertises { get; set; }

    }
}