﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HRMVC.Models
{
    public class StatusViewModel
    {
        public int StatusID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public int Priority { get; set; }
    }
}