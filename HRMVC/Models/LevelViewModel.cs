﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HRMVC.Models
{
    public class LevelViewModel
    {
        public int LevelID { get; set; }
        [Required]
        public string Name { get; set; }
    }
}