﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HRMVC.Models
{
    public class ExpertiseLevelViewModel
    {
        [Required(ErrorMessage = "Expertise ID is required")]
        public int ExpertiseID { get; set; }

        [Required(ErrorMessage = "IsPrimary field is required")]
        public bool IsPrimary { get; set; }

        [Required(ErrorMessage = "LevelID field is required")]
        public int LevelID { get; set; }

        public string ExpertiseName { get; set; }
        public string LevelName { get; set; }
    }
}