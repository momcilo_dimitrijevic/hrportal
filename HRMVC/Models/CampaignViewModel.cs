﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HRMVC.Models
{
    public class CampaignViewModel
    {
        public CampaignViewModel()
        {
            AssignedCandidates = new List<AssignedCandidateViewModel>();
        }

        [Required]
        public int CampaignID { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Start Date is required")]
        public DateTime DateStarted { get; set; }

        [Required(ErrorMessage = "Finish Date is required")]
        public DateTime DateFinished { get; set; }

        [Required]
        [DataType(DataType.Text)]
        public string Description { get; set; }

        public List<AssignedCandidateViewModel> AssignedCandidates { get; set; }

        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public string LastUpdatedBy { get; set; }
    }
}