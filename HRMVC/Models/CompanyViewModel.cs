﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using HRMVC.Helpers;

namespace HRMVC.Models
{
    public class CompanyViewModel
    {
        public int CompanyID { get; set; }

        [Required(ErrorMessage = "Company name is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Company address is required")]
        public string Address { get; set; }

        [RegularExpression(DisplayPatterns.UrlPattern, ErrorMessage = DisplayPatterns.UrlPatternMessage)]
        public string Website { get; set; }

        [Required(ErrorMessage = "Telephone number is required")]
        [RegularExpression(DisplayPatterns.PhonePattern, ErrorMessage = DisplayPatterns.PhonePatternMessage)]
        public string TelNum { get; set; }

        public System.DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> Deleted { get; set; }
        public string DeletedBy { get; set; }
    }
}