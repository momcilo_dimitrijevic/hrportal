﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRMVC.Helpers
{
    public static class DisplayPatterns
    {
        public const string PhonePattern = @"^\(?([0-9]{3})\)?[/. ]?([0-9]{3})[-. ]?([0-9]{3,4})$";
        public const string PhonePatternMessage = "Wrong phone number format (example: 012345678, 012/345-678, (012)/345-6789)";
        public const string UrlPattern = @"^(http(s)?:\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$";
        public const string UrlPatternMessage = "Invalid URL format";
    }
}