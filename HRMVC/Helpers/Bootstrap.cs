﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRMVC.Helpers
{
    public class Bootstrap
    {
        public const string BundleBase = "~/Content/css/";

        public class Theme
        {
            public const string cerulean = "cerulean";
            public const string cosmo = "cosmo";
            public const string cyborg = "cyborg";
            public const string darkly = "darkly";
            public const string flatly = "flatly";
            public const string journal = "journal";
            public const string lumen = "lumen";
            public const string paper = "paper";
            public const string readable = "readable";
            public const string sandstone = "sandstone";
            public const string simplex = "simplex";
            public const string slate = "slate";
            public const string spacelab = "spacelab";
            public const string superhero = "superhero";
            public const string stock = "stock";
            public const string united = "united";
            public const string yeti = "yeti";
        }


        public static HashSet<string> Themes = new HashSet<string>
        {
            Theme.cerulean,
            Theme.cosmo,
            Theme.cyborg,
            Theme.darkly,
            Theme.flatly,
            Theme.journal,
            Theme.lumen,
            Theme.paper,
            Theme.readable,
            Theme.sandstone,
            Theme.simplex,
            Theme.slate,
            Theme.spacelab,
            Theme.superhero,
            Theme.stock,
            Theme.united,
            Theme.yeti
        };

        public static string Bundle(string themename)
        {
            return BundleBase + themename;
        }
    }
}