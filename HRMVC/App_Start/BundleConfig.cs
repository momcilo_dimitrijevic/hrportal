﻿using System.Web;
using System.Web.Optimization;
using HRMVC.Helpers;

namespace HRMVC
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            foreach (var theme in Bootstrap.Themes)
            {
                var stylePath = string.Format("~/Content/bootswatch/{0}/bootstrap.css", theme);

                bundles.Add(new StyleBundle(Bootstrap.Bundle(theme)).Include(
                            stylePath,
                            "~/Content/bootstrap.custom.css",
                            "~/Content/site.css"));
            }

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));
            //"~/Scripts/jquery.contextmenu.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jqueryvalidate/jquery.validate.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
            "~/Scripts/jquery-ui-{version}.js"));


            bundles.Add(new StyleBundle("~/Content/css").Include(
                //"~/Content/bootswatch/flatly/bootstrap.css",
                "~/Content/site.css"
                ));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                //"~/Content/themes/base/all.css"
                "~/Content/themes/jquery-ui-1.10.0.custom.css"
              ));

            bundles.Add(new ScriptBundle("~/bundles/fileinputscript").Include(
                     "~/Scripts/fileinput.min.js"));

            bundles.Add(new StyleBundle("~/bundles/fileinputcss").Include(
                     "~/Content/fileinput.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/multiselectScript").Include(
                     "~/Scripts/bootstrap-multiselect.js"));

            bundles.Add(new StyleBundle("~/bundles/multiselectStyle").Include(
                            "~/Content/bootstrap-multiselect.css"));

            bundles.Add(new ScriptBundle("~/bundles/dataTablesScript").Include(
                     "~/Scripts/DataTables/datatables.min.js"
                     ));

            bundles.Add(new StyleBundle("~/bundles/dataTablesCSS").Include(
                      "~/Scripts/DataTables/datatables.min.css",
                      "~/Scripts/DataTables/datatables-custom.css"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrapNotify").Include(
                                        "~/Scripts/bootstrap-notify.min.js"));

            bundles.Add(new StyleBundle("~/bundles/animate").Include(
                                    "~/Content/animate.css"));
        }
    }
}
